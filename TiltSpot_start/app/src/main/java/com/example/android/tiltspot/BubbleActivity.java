package com.example.android.tiltspot;

import android.hardware.Sensor;
import android.hardware.SensorEvent;
import android.hardware.SensorEventListener;
import android.hardware.SensorManager;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

public class BubbleActivity extends AppCompatActivity implements SensorEventListener {

    private ImageView mBubbleImage;
    private TextView mTextXvalue;
    private TextView mTextYvalue;


    private SensorManager mSensorManager;
    private Sensor mSensorAccelerometer;
    private Sensor mSensorMagnetometer;

    //get sensor data
    private float[] mSensorAccelerometerData = new float[9];
    private float[] mSensorMagnetometerData = new float[9];

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_bubble);

        mBubbleImage = findViewById(R.id.image_bubble);
        mTextXvalue  = findViewById(R.id.textView_x_value);
        mTextYvalue = findViewById(R.id.textView_y_value);
        //initialize sensor manager
        mSensorManager = (SensorManager) getSystemService(SENSOR_SERVICE);

        //get the sensor from the SensorManager
        mSensorAccelerometer = mSensorManager.getDefaultSensor(Sensor.TYPE_ACCELEROMETER);
        mSensorMagnetometer = mSensorManager.getDefaultSensor(Sensor.TYPE_MAGNETIC_FIELD);

        //check sensor is available in the device
        if(mSensorAccelerometer == null){
            Toast.makeText(this, "Required Accelerometer Sensor not found", Toast.LENGTH_SHORT).show();
        }

        if(mSensorMagnetometer == null){
            Toast.makeText(this, "Required Magnetometer Sensor not found", Toast.LENGTH_SHORT).show();
        }

    }

    @Override
    public void onSensorChanged(SensorEvent event) {
        int sensorType = event.sensor.getType();
        float mAccelerometerX = 0;
        float mAccelerometerY = 0;
        switch (sensorType){
            case Sensor.TYPE_ACCELEROMETER:
                mAccelerometerX -= event.values[0];
                mAccelerometerY += event.values[1];
                break;
            case Sensor.TYPE_MAGNETIC_FIELD:
                mSensorMagnetometerData = event.values.clone();
                break;
            default:
                return;
        }
        float x = mBubbleImage.getX() + mAccelerometerX;
        float y = mBubbleImage.getY() + mAccelerometerY;
        mBubbleImage.setX(x);
        mBubbleImage.setY(y);

        mTextXvalue.setText(getResources().getString(R.string.value_format,x));
        mTextYvalue.setText(getResources().getString(R.string.value_format,y));

    }

    @Override
    public void onAccuracyChanged(Sensor sensor, int accuracy) {

    }

    @Override
    protected void onStart() {
        super.onStart();
        if (mSensorAccelerometer != null)
            mSensorManager.registerListener(this,mSensorAccelerometer,SensorManager.SENSOR_DELAY_NORMAL);
        if (mSensorMagnetometer != null)
            mSensorManager.registerListener(this,mSensorMagnetometer,SensorManager.SENSOR_DELAY_NORMAL);
    }

    @Override
    protected void onStop() {
        super.onStop();

        if(mSensorAccelerometer != null){
            mSensorManager.unregisterListener(this);
        }
        if(mSensorMagnetometer != null){
            mSensorManager.unregisterListener(this);
        }
    }

}

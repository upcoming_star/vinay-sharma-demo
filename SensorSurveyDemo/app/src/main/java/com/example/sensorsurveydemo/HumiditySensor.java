package com.example.sensorsurveydemo;

import androidx.appcompat.app.AppCompatActivity;

import android.hardware.Sensor;
import android.hardware.SensorEvent;
import android.hardware.SensorEventListener;
import android.hardware.SensorManager;
import android.os.Bundle;
import android.widget.TextView;

public class HumiditySensor extends AppCompatActivity implements SensorEventListener {

    private SensorManager mSensorManager;
    private Sensor mSensorHumidity;
    private TextView mHumidityText;



    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_humidity_sensor);

        mHumidityText = findViewById(R.id.text_humidity);

        mSensorManager = (SensorManager) getSystemService(SENSOR_SERVICE);
        mSensorHumidity = mSensorManager.getDefaultSensor(Sensor.TYPE_RELATIVE_HUMIDITY);

        String errorMessage = getResources().getString(R.string.error_no_sensor);

        if(mSensorHumidity == null){
            mHumidityText.setText(errorMessage);
        }
    }

    @Override
    public void onSensorChanged(SensorEvent event) {
        int sensorType = event.sensor.getType();
        float sensorData = event.values[0];

        switch (sensorType){
            case Sensor.TYPE_RELATIVE_HUMIDITY:
                mHumidityText.setText(getResources().getString(R.string.humidity_sensor, sensorData));
                break;
            default://nothing
        }
    }

    @Override
    public void onAccuracyChanged(Sensor sensor, int accuracy) {

    }

    @Override
    protected void onResume() {
        super.onResume();
        mSensorManager.registerListener(this,mSensorHumidity,SensorManager.SENSOR_DELAY_NORMAL);
    }

    @Override
    protected void onPause() {
        super.onPause();
        mSensorManager.unregisterListener(this);
    }
}

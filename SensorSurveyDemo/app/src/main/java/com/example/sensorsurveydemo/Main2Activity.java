package com.example.sensorsurveydemo;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.hardware.Sensor;
import android.hardware.SensorEvent;
import android.hardware.SensorEventListener;
import android.hardware.SensorManager;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;

public class Main2Activity extends AppCompatActivity implements SensorEventListener {

    private Sensor mSensorProximity;
    private Sensor mSensorLight;
    private SensorManager mSensorManager;
    private TextView mTextProximity;
    private TextView mTextLight;
    private ImageView mImageView;
    private Button nextButton;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main2);

        mTextLight = findViewById(R.id.label_light);
        mTextProximity = findViewById(R.id.label_proximity);
        mImageView = findViewById(R.id.image_view);
        nextButton = findViewById(R.id.next_activity_two_button);
        mSensorManager = (SensorManager) getSystemService(SENSOR_SERVICE);

        mSensorProximity = mSensorManager.getDefaultSensor(Sensor.TYPE_PROXIMITY);
        mSensorLight = mSensorManager.getDefaultSensor(Sensor.TYPE_LIGHT);

        String error_message = getResources().getString(R.string.error_no_sensor);

        if(mSensorLight == null){
            mTextLight.setText(error_message);
        }
        if(mSensorProximity == null){
            mTextProximity.setText(error_message);
        }

        nextButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                startActivity(new Intent(Main2Activity.this, HumiditySensor.class));
            }
        });
    }

    @Override
    public void onSensorChanged(SensorEvent event) {
        int sentorType = event.sensor.getType();
        float sensorData = event.values[0];

        View view = findViewById(R.id.root_layout);
        View root = view.getRootView();

        switch (sentorType){
            case Sensor.TYPE_LIGHT:
                mTextLight.setText(getResources().getString(R.string.label_light, sensorData));
                if(sensorData <= 50.0){
                    root.setBackgroundResource(R.color.colorRed);
//                    getWindow().getDecorView().setBackgroundResource(R.color.colorRed);
                }
                else if(sensorData >=50 && sensorData <= 100){
                    root.setBackgroundResource(R.color.colorBlue);
                }
                else if(sensorData >= 100){
                    root.setBackgroundResource(R.color.colorYellow);
                }
                break;
            case Sensor.TYPE_PROXIMITY:
                mTextProximity.setText(getResources().getString(R.string.proximity_sensor, sensorData));
                if(sensorData == 0.00){
                    mImageView.getLayoutParams().height = 100;
                    mImageView.getLayoutParams().width = 100;
                }else if(sensorData == 5.00){
                    mImageView.getLayoutParams().width = 500;
                    mImageView.getLayoutParams().height = 5800;
                }
                break;
            default://nothing
        }
    }

    @Override
    public void onAccuracyChanged(Sensor sensor, int accuracy) {

    }

    @Override
    protected void onStart() {
        super.onStart();

        if(mSensorProximity != null){
            mSensorManager.registerListener(this,mSensorProximity,SensorManager.SENSOR_DELAY_NORMAL);
        }

        if(mSensorLight != null){
            mSensorManager.registerListener(this,mSensorLight,SensorManager.SENSOR_DELAY_NORMAL);
        }
    }

    @Override
    protected void onStop() {
        super.onStop();

        mSensorManager.unregisterListener(this);
    }
}
